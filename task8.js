// თვქენი დავალებაა გააკეთოთ character  ების კლასი სადაც შეინახავთ შემდეგ ინფორმაციას:
//  სახელი და როლი (როლი შეიძლება იყოს რამოდენიმენაირი: mage, support, assassin, adc,
//  tank (არ არის აუცილებელი ამ ჩამონათვლის სადმე შენახვა ან ყველა როლის გამოყენება)პ0ო,
// აქედან support mage  - ც არის. ყველა პერსონაჟს სახელთან და როლთან ერთად აქვს armor ისა და damage ის მეთოდები,
// რომლებიც უბრალოდ ლოგავენ თუ რამდენი არმორი აქვს და რამდენი dmg აქვს პერსონაჟს. damage ის შემთხვევაში შეგვიძლია გვააკეთოთ შემოწმება mage,
//  support ზე და სხვა როლებზე, პირველი ორის შემთხვევაში პერსონაჟი magic damage ს აკეთებს, ხოლო სხვა დანარჩენ შემთვევებში attack damage და
//  მეთოდიც ამას ლოგავს.  mage ის კლასი ასევე მიიღებს magicPowers და აქვს ფრენის მეთოდიც (mage -ები დაფრინავენ კიდეც), რომელიც დალოგავს,
//  რომ ამ მეიჯს შეუძლია ფრენაც. support იგივე მეიჯია, რომელიც მიიღებს heals - ს (თუ რამდენი "ერთეულის" დაჰილვა შეუძლია) ცვლადად და
//   ექნნება  healing მეთოდი რომელიც დალოგავს, რომ ამ გმირს შეუძლია გარკვეული რაოდენობის ერთეულის დაჰილვა. ასევე გვყავს adc რომელიც მიიღებს
//   attackdamages ს და აქვს მეთოდი, რომელიც ითვლის მის რეინჯს (მეთოდმა რეინჯის მნიშვნელობა უნდა მიიღოს), ამაზე დაყრდნობით ის ლოგავს ახლო
//   რეინჯიანია პერსონაჟი თუ არა. (პირობითად ვთქვათ, რომ 30 < ახლო რეინჯში ითვლება)

// რაც შეეხება თვითონ პერსონაჟების სახელებს და ყველა იმ მნიშვნელობებს რომლებიც კლასებს უნდა გადასცეთ its up to you.
// თქვენი ამოცანა კლასების შექმნაში და მათი სტრუქტურის სწორ აგებულებაში მდგომარეობს.

class Character {
    
  constructor(name, role) {
    this.name = name;
    this.role = role;
  }

  armorNumber() {
    console.log(`${this.name} Has ${this.armor} Armor`);
  }

  damageNumber() {
    console.log(`${this.name} Has ${this.magicDamage} Damage`);
  }
}

class Mage extends Character {
    armor = 0 
  magicDamage = 0;
  constructor(name, role, magicDamage, armor) {
    super(name, role);
    this.magicDamage = magicDamage;
    this.armor = armor
  }
  magicPowers() {
    console.log(
      `${this.name} Can flyyy And Has A MagicDamage Of ${this.magicDamage}`
    );
  }
}

class Support extends Mage {
  armor = 0;
  magicDamage = 0;
  Healing = 0;
  constructor(name, role, magicDamage, Healing, armor) {
    super(name, role);
    this.magicDamage = magicDamage;
    this.Healing = Healing;
    this.armor = armor;
  }
  magicPowers() {
    console.log(
      `${this.name} Can flyyy And Has A MagicDamage Of ${this.magicDamage}`
    );
  }
  Healing() {
    console.log(
      `${this.name} Is A Support And Has A Healing Power Of ${this.Healing}`
    );
  }
}

class ADC extends Character {
  armor = 0;
  rangeLength = 0;
  AttackDamage = 0;
  constructor(name, role, AttackDamage, rangeLength, armor) {
    super(name, role);
    this.AttackDamage = AttackDamage;
    this.rangeLength = rangeLength;
    this.armor = armor;
  }
  Range() {
    if (this.rangeLength > 30) {
      console.log(
        `${this.name} Is Ranged Champ And Has A Range of  ${this.rangeLength}`
      );
    } else {
      console.log(
        `${this.name} Is Not A Ranged Champ And Has A Range of  ${this.rangeLength}`
      );
    }
  }
}

let varus = new ADC("Varus", "ADC", 330, 550, 70);

let xerath = new Mage("Xerath", "Mage", 200, 50);

let bard = new Support("Bard", "Support", 100, 320, 130);

console.log(varus);
console.log(xerath);
console.log(bard);

varus.Range();
xerath.magicPowers();
